import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerOutput implements Runnable{
    public ServerOutput(String newip, String newoperation){
        ip.add(newip.substring(1,newip.indexOf(":")));
        operation.add(newoperation);
    }
    public static List<String> ip = new ArrayList<String>();
    public static List<String> operation = new ArrayList<String>();
    private String datatype;
    private String dataammount;

    public void run(){
        detect();
    }
    public void detect(){
        try {
            ServerSocket serverSocket = new ServerSocket(15005);
            Socket socket = serverSocket.accept();
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            int H = ip.indexOf(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":")));
            switch(operation.get(H)){
                case "LOGIN":
                    writer.println("LOGINOK");
                    writer.println(sIDgen.getsID());
                    writer.println(LoggingIn.getPrivateId(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":"))));
                    break;
                case "LOGINREJECTION":
                    writer.println("LOGINREJECTION");
                    writer.println(datatype);
                    writer.println();
                    writer.println(LoggingIn.getPrivateId(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":"))));
                    break;
                case "GETSTRING":
                    writer.println("GETSTRINGOK");
                    writer.println(LoggingIn.getPrivateId(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":"))));
                    break;
                case "GETSTRINGREJECTION":
                    System.out.println("GETSTRINGREJECTION");
                    writer.println(LoggingIn.getPrivateId(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":"))));
                    break;
                case "LOGOUT":
                    System.out.println("LOGOUTOK");
                    writer.println(LoggingIn.getPrivateId(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":"))));
                    break;
                case "LOGOUTREJECTION":
                    System.out.println("LOGOUTREJECTION");
                    writer.println(LoggingIn.getPrivateId(socket.getRemoteSocketAddress().toString().substring(1,socket.getRemoteSocketAddress().toString().indexOf(":"))));
                    break;
            }
            writer.flush();
            writer.close();
            socket.close();
            serverSocket.close();
        }catch(Exception e){}
    }
}





import java.util.Scanner;

public class InstructionsListener implements Runnable{
    //VAR
    private long workingtimev=System.currentTimeMillis();
    //VAR-END
    //MET
    public void run(){
        getInstruction();
    }
    public void getInstruction(){
        Scanner scanner = new Scanner(System.in);
        for(int H=1;H>0;){
            System.out.print("SERVER# ");
            instructionExecute(scanner.nextLine());
        }
    }
    public void instructionExecute(String instruction){
        OtherServices otherServices = new OtherServices();
        Scanner scanner = new Scanner(System.in);
        switch(instruction){
            case "listeningon":
                otherServices.serverInputStart();
                System.out.println("SERVER# LISTENINGON: WYKONANO");
                try {
                    Thread.currentThread().sleep(50);
                }catch(InterruptedException e){}
                break;
            case "listeningkill":
                otherServices.serverInputKill();
                System.out.println("SERVER# LISTENINGKIL: WYKONANO");
                break;
            case "gethostaddress":
                otherServices.getHostAddress();
                System.out.println("SERVER# "+instruction+": WYKONANO");
                break;
            case "getworkingtime":
                time();
                break;
            case "getcurrentgeneratedsid":
                otherServices.getCurrentGeneratedsID();
                break;
            case "getcurrentgeneratedsidlast":
                otherServices.getCurrentGeneratedsIDlast();
                break;
            case "getcurrentconnectedusers":
                otherServices.getCurrentConnectedUsers();
                break;
            case "getusersproperties":
                otherServices.getUsersProperties();
                break;
            case "executeforcelogout":
                System.out.print("SERVER# podaj nazwe uzytkownika: ");
                otherServices.executeForceLogout(scanner.nextLine());
                break;
            case "getusersaddresses":
                otherServices.getUsersAddresses();
                break;
            default:
                System.out.println("SERVER# polecenie nie rozpoznane");
        }
    }
    public void time(){
        double time1,time2,time3,time4;
        long timecheck=System.currentTimeMillis();
        time1=(timecheck-workingtimev)/1000;
        time2=time1%60;
        time3=time1/60;
        time4=(time3-(time2/60));
        time1=time1-time4*60;
        System.out.println("SERVER# Server dziala od: "+time4+" minut | "+ time1 +" sekund");
    }
    //MET-END
}

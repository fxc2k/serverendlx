import java.net.InetAddress;

public class OtherServices extends LoggingIn {
    private int portH=15001;
    //CON
    /**Method responsible for **/
    public OtherServices() {
        serverinputrunn1 = new PreServerInput0(15001);
        serverinputthr1 = new Thread(serverinputrunn1);
        serverinputrunn1 = new PreServerInput0(getport());
        serverinputthr2 = new Thread(serverinputrunn1);
        serverinputrunn1 = new PreServerInput0(getport());
        serverinputthr3 = new Thread(serverinputrunn1);
    }
    //CON-END
    //VAR
    private Runnable serverinputrunn1;
    private Thread serverinputthr1, serverinputthr2, serverinputthr3;
    //VAR-END
    //MET
    public void serverInputStart() {
        serverinputthr1.start();
        serverinputthr2.start();
        serverinputthr3.start();
        try {
            Thread.currentThread().sleep(50);
        } catch (InterruptedException e) {
        }
    }
    public void serverInputKill() {
        serverinputthr1.stop();
    }
    public void getHostAddress() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            System.out.println("SERVER# ADDRESS:" + address);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void getCurrentGeneratedsID() {
        System.out.println("SERVER# sID: " + sIDgen.getsID());
    }
    public void getCurrentGeneratedsIDlast() {
        System.out.println("SERVER# sIDlast: " + sIDgen.getsIDlast());
    }
    public void getUsersProperties(){
        int quantity = user.size() - 1;
        for (; quantity > -1; ) {
            System.out.print("SERVER#");
            System.out.println("User: "+user.get(quantity)+" UserIP: "+ userip.get(quantity)+" PrivateID: "+ userprivateid.get(quantity));
            quantity--;
        }
    }
    public void getCurrentConnectedUsers() {
        int quantity = user.size() - 1;
        System.out.println("SERVER# ");
        System.out.println("Users: ");
        for (; quantity > -1; ) {
            System.out.println(user.get(quantity));
            quantity--;
        }
    }
    public void getUsersAddresses(){
        int quantity = user.size() - 1;
        System.out.println("SERVER# ");
        for (; quantity > -1; ) {
            System.out.println("User: "+user.get(quantity)+" UserIP: "+ userip.get(quantity));
            quantity--;
        }
    }
    public void executeForceLogout(String username){
        int number = user.indexOf(username);
        user.remove(number);
        userip.remove(number);
        userprivateid.remove(number);
        userstatus.remove(number);
    }
    private int getport(){
        if(portH==15001){
            portH=15011;
            return 15011;}
        if(portH==15011){
            portH=150012;
            return 15012;}
        return 0;
    }
    //MET-END
}

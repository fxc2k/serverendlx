public class Logout extends LoggingIn implements Runnable{
    private String userI,sidI,privateidI,useripI;
    public Logout(String userS,String sidS,String privateidS, String useripS){
        userI=userS;
        sidI=sidS;
        privateidI=privateidS;
        useripI=useripS;
    }
    public void run(){
        logout();
    }
    public void logout(){
        sIDgen sIDgen = new sIDgen();
        String mixedid=privateidI+sidI;
        int index=user.indexOf(userI);
        if(index!=-1){
            boolean zgodnoscmixedid=false;
            if(mixedid.equals(userprivateid.get(index)+sIDgen.getsID())){zgodnoscmixedid=true;}
            if(mixedid.equals(userprivateid.get(index)+sIDgen.getsIDlast())){zgodnoscmixedid=true;}
            if(zgodnoscmixedid==true){
                user.remove(index);
                userip.remove(index);
                userstatus.remove(index);
                userprivateid.remove(index);
                ServerOutput serverOutput = new ServerOutput(useripI,"LOGOUT");
            }
            else{
                ServerOutput serverOutput = new ServerOutput(useripI,"LOGOUTREJECTION");
            }
        }
        else{
            ServerOutput serverOutput = new ServerOutput(useripI,"LOGOUTREJECTION");
        }
    }
}

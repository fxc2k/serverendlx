import java.net.Socket;
import java.net.ServerSocket;

public class PreServerInput0 implements Runnable{
    private int port;
    public PreServerInput0(int port){
        this.port=port;
    }
    public void run(){
        while(true){
            try {
                ServerSocket serverSocket = new ServerSocket(port);
                Socket sock= serverSocket.accept();
                new ServerInput(sock).start();
            }catch(Exception e){}
        }
    }
}

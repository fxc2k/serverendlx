import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class sIDsender extends Thread implements Runnable{
    private String idoperacji, user, privateid, sid;
    private int port;
    private Socket sock;
    public sIDsender(Socket klientsocket,int portS){
        this.sock=klientsocket;
        this.port=portS+10;
    }
    public void run(){
        String privateid, sid;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            idoperacji=reader.readLine();
            if(idoperacji.equals("SIDREQUEST")){
                user = reader.readLine();
                privateid = reader.readLine();
                sid = sIDgen.getsID();
                reader.close();
                sock.close();
                if (sid.equals(sIDgen.getsID()) && privateid.equals(LoggingIn.getPrivateId(user, true))) {
                    ServerSocket serverSocket = new ServerSocket(port + 10);
                    Socket socket = serverSocket.accept();
                    PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                    writer.println(LoggingIn.getPrivateId(user, true));
                    writer.println(sIDgen.getsID());
                    LoggingIn.setStatus(socket.getRemoteSocketAddress().toString().substring(1, socket.getRemoteSocketAddress().toString().indexOf(":")));
                    writer.close();
                    socket.close();
                    serverSocket.close();

                }
                if (sid.equals(sIDgen.getsIDlast()) && privateid.equals(LoggingIn.getPrivateId(user, true))) {
                    ServerSocket serverSocket = new ServerSocket(port + 10);
                    Socket socket = serverSocket.accept();
                    PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                    writer.println(LoggingIn.getPrivateId(user, true));
                    writer.println(sIDgen.getsID());
                    LoggingIn.setStatus(socket.getRemoteSocketAddress().toString().substring(1, socket.getRemoteSocketAddress().toString().indexOf(":")));
                    writer.close();
                    socket.close();
                    serverSocket.close();
                }
            }
        }catch(Exception e){}
    }
}


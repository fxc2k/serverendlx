import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.sql.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ServerInput extends Thread implements Runnable{
    //VAR
    private String typoperacji, idpolaczenia, hostsql, passsql, logsql;
    private  Runnable runthr1;
    private Thread thr1;
    private Socket sock;
    //VAR-END
    //CON
    public ServerInput(){}
    public ServerInput(Socket klientsocket){
        this.sock=klientsocket;
    }
    //CON-END
    //MET
    public void run() {
        serwerinput();
    }
    public void serwerinput() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            typoperacji = reader.readLine();
            idpolaczenia = reader.readLine();
            switch(typoperacji){
                case "LOGIN":
                    String user1, passwd, userip, userprivateid;
                    user1=reader.readLine();
                    passwd=reader.readLine();
                    userprivateid=reader.readLine();
                    userip=sock.getRemoteSocketAddress().toString();
                    logs(user1);
                    runthr1 = new LoggingIn(user1,passwd,userip,userprivateid);
                    thr1 = new Thread(runthr1);
                    thr1.start();
                    break;
                case "LOGOUT":
                    String user2, sid2, privateid2;
                    user2=reader.readLine();
                    sid2=reader.readLine();
                    privateid2=reader.readLine();
                    logs(user2);
                    runthr1 = new Logout(user2,sid2,privateid2,userip=sock.getRemoteSocketAddress().toString());
                    thr1 = new Thread(runthr1);
                    thr1.start();
                    break;
                case "GETSTRING":
                    String user3, privateid3,sid3;
                    user3 = reader.readLine();
                    privateid3 = reader.readLine();
                    sid3 = reader.readLine();
                    logs(user3);
                    thr1 = new Thread(runthr1);
                    thr1.start();
                    //WYWOLAC ODPOWIEDNI WATEK
                    break;
                default: break;
            }
            reader.close();
            sock.close();
        } catch (SocketException e) {
            System.out.println("SERVER#[INPUT] SOCKET ERROR");
        } catch (IOException e) {
            System.out.println("SERVER#[INPUT] IOEXCEPTION");
        } catch (Exception e) {
            System.out.println("SERVER#[INPUT] OTHER EXCEPTION");
        }
    }
    public void logs(String username){
        try{
            final String type= "INPUT";
            SetSQLConnectionProperties();
            Connection sqlconnection = DriverManager.getConnection(hostsql,logsql,passsql);
            Statement statement = sqlconnection.createStatement();
            statement.executeUpdate("INSERT INTO logs (Username,Operation,InputOutput,InternalConnectionID) VALUES ('"+username+"','"+typoperacji+"','"+type+"','"+idpolaczenia+"')");
            statement.close();
            sqlconnection.close();
        }catch(SQLException E){
            System.out.println("SERVER# SQL EXCEPTION: "+ E);
        }
    }
    private void SetSQLConnectionProperties(){
        File file = new File("sqlproperties.txt");
        String[] properties = new String[3];
        try{
            Scanner SC1 = new Scanner(file);
            StringTokenizer token;
            int H=0;
            while(SC1.hasNextLine()){
                token = new StringTokenizer(SC1.nextLine(),",");
                while(token.hasMoreElements()){
                    properties[H] = token.nextToken();
                    H++;
                }
            }
        }catch(Exception e){
            System.out.println("Scanner File Exception: "+e);
        }
        hostsql=properties[0];
        logsql=properties[1];
        passsql=properties[2];
    }
    //MET-END
}

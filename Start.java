public class Start{
        public static void main(String[] args){

            BasicsServices basicsServices = new BasicsServices();
            basicsServices.servicesIDgenStart();
            /**sIDgen class start**/
            basicsServices.servicesIDsenderStart();
            /**Responsible for sending current sID for client**/
            basicsServices.serviceStatusCheckerStart();
            /**Responsible for checking user status(online/offline)**/
            basicsServices.serviceInstructionsListenerStart();
            /**Responsible for listening commands**/

        }
}

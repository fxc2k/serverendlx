import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class LoggingIn implements Runnable{
    //VAR
    private String tempuser, tempuserip, tempuserprivateid, tempuserpasswd;
    private static String hostsql, logsql, passsql;
    public static List<String> user = new ArrayList<String>();
    public static List<String> userip = new ArrayList<String>();
    public static List<String> userprivateid = new ArrayList<String>();
    public static List<Boolean> userstatus = new ArrayList<Boolean>();
    //VAR-END
    //CON
    public LoggingIn(){}
    public LoggingIn(String userI,String userpasswd,String useripI,String userprivateid){
        tempuser=userI;
        tempuserpasswd=userpasswd;
        tempuserip=useripI;
        tempuserprivateid=userprivateid;
        SetSQLConnectionProperties();
    }
    //CON-END
    //MET
    public void run(){
        operationLogin();
    }
    private void operationLogin(){
        String passwdfromsql="";
        try{
            Connection sqlconnection = DriverManager.getConnection(hostsql,logsql,passsql);
            Statement statement = sqlconnection.createStatement();
            ResultSet RS = statement.executeQuery("SELECT * FROM acc WHERE login='"+tempuser+"'");
            while(RS.next()){
                passwdfromsql = RS.getString(3);
            }
            statement.close();
            sqlconnection.close();
        }catch(SQLException E){
            System.out.println("SERVER# SQL EXCEPTION: "+ E);
        }
        passwdfromsql="a";
        tempuserpasswd=passwdfromsql;
        if(tempuserpasswd.equals(passwdfromsql)){
            if(user.contains(tempuser)){
                int point;
                point=user.indexOf(tempuser);
                userprivateid.set(point,tempuserprivateid);
                ServerOutput serverOutput = new ServerOutput(tempuserip, "CHANGELOGIN");
                serverOutput.detect();
            }
            else{
                int listsize;
                user.add(tempuser);
                listsize = user.size() - 1;
                userip.add(listsize, tempuserip.substring(1, tempuserip.indexOf(":")));
                userstatus.add(listsize, true);
                userprivateid.add(listsize, tempuserprivateid);
                System.out.println("SERVER# ZALOGOWANO: " + tempuser);
                ServerOutput serverOutput = new ServerOutput(tempuserip, "LOGIN");
                serverOutput.detect();
            }

        }
        else{
            System.out.println("SERVER# NIEZALOGOWANO: "+ tempuser);
            ServerOutput serverOutput = new ServerOutput(tempuserip, "LOGINREJECTION");
            serverOutput.detect();
        }
    }
    private void SetSQLConnectionProperties(){
        File file = new File("sqlproperties.txt");
        String[] properties = new String[3];
        try{
            Scanner SC1 = new Scanner(file);
            StringTokenizer token;
            int H=0;
            while(SC1.hasNextLine()){
                token = new StringTokenizer(SC1.nextLine(),",");
                while(token.hasMoreElements()){
                    properties[H] = token.nextToken();
                    H++;
                }
            }
        }catch(Exception e){
            System.out.println("Scanner File Exception: "+e);
        }
        hostsql=properties[0];
        logsql=properties[1];
        passsql=properties[2];
    }
    public static String getPrivateId(String ip){
        int point = userip.indexOf(ip);
        return userprivateid.get(point);
    }
    public static String getPrivateId(String user1, boolean W1){
        return userprivateid.get(user.indexOf(user1));
    }
    public static void setStatus(String ip){
        userstatus.set(userip.indexOf(ip),true);
    }
    //MET-END
}

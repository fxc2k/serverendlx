public class BasicsServices{

    //VAR
    private static Runnable runnableIDsender, runnableinstructionslistener, runnablesIDgen, runnablestatuschecker;
    private static Thread threadIDsender0,threadIDsender1,threadIDsender2,threadinstructionslistener, threadsIDgen, threadstatuschecker;
    /**Port number(used like argument in PresIDsender0)**/
    private static int portH=15020;
    //VAR-END
    //MET
    /**Responsible for start generating sID code**/
    public static void servicesIDgenStart(){
        runnablesIDgen = new sIDgen();
        threadsIDgen = new Thread(runnablesIDgen);
        threadsIDgen.start();
    }
    /**Responsible for responding on client requests(sending sid code)**/
    public static void servicesIDsenderStart(){
        runnableIDsender = new PresIDsender0(portH);
        threadIDsender0 = new Thread(runnableIDsender);
        threadIDsender0.start();
        getport();
        runnableIDsender = new PresIDsender0(portH);
        threadIDsender1 = new Thread(runnableIDsender);
        threadIDsender1.start();
        getport();
        runnableIDsender = new PresIDsender0(portH);
        threadIDsender2 = new Thread(runnableIDsender);
        threadIDsender2.start();
    }
    /**Responsible for listening commands**/
    public static void serviceInstructionsListenerStart(){
        runnableinstructionslistener = new InstructionsListener();
        threadinstructionslistener = new Thread(runnableinstructionslistener);
        threadinstructionslistener.start();
    }
    /**Responsible for checking user status(online or offline)**/
    public static void serviceStatusCheckerStart(){
        runnablestatuschecker = new StatusChecker();
        threadstatuschecker = new Thread(runnablestatuschecker);
        threadstatuschecker.start();
    }
    /**Responsible for changing variable "portH"**/
    private static int getport(){
        if(portH==15020){
            portH=15021;
            return 15021;}
        if(portH==15021){
            portH=15022;
            return 15022;
        }
        return 0;
    }
    //MET-END
}

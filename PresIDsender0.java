import java.net.ServerSocket;
import java.net.Socket;

public class PresIDsender0 implements Runnable{
    private int port;
    public PresIDsender0(int port){
        this.port=port;
    }
    public void run(){
        while(true){
            try {
                ServerSocket serverSocket = new ServerSocket(port);
                Socket sock= serverSocket.accept();
                new sIDsender(sock, port).start();
                sock.close();
                serverSocket.close();
            }catch(Exception e){
                System.out.println(e);
            }
        }
    }
}

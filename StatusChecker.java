public class StatusChecker extends LoggingIn implements Runnable{
    public void run(){statuscheck();}
    public void statuscheck(){
        while(true){
            int indexofreturn;
            do{
                indexofreturn = userstatus.indexOf(false);
                if(indexofreturn != -1) {
                    user.remove(indexofreturn);
                    userip.remove(indexofreturn);
                    userprivateid.remove(indexofreturn);
                    userstatus.remove(indexofreturn);
                }
            }while(indexofreturn != -1);
            do{
                indexofreturn = userstatus.indexOf(true);
                if(indexofreturn != -1){
                    userstatus.set(indexofreturn, false);
                }
            }while(indexofreturn != -1);
            try{
                Thread.currentThread().sleep(120000);
            }catch(Exception e){
                System.out.println("StatusCheck Exception:" +e);
            }
        }
    }
}

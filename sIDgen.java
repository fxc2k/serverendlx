import java.util.Random;

public class sIDgen implements Runnable{

    static public String sID;
    static public String sIDlast;

    public void run(){
        for(int H=1;H>0;){
            genid();
        }
    }
    public static void genid(){
        String id = "";
        char genID[] = new char[35];
        genID[0] = 'A';
        genID[1] = 'B';
        genID[2] = 'C';
        genID[3] = 'Q';
        genID[4] = 'W';
        genID[5] = 'E';
        genID[6] = 'T';
        genID[7] = 'Y';
        genID[8] = 'U';
        genID[9] = 'I';
        genID[10] = 'P';
        genID[11] = 'K';
        genID[12] = 'J';
        genID[13] = 'H';
        genID[14] = 'G';
        genID[15] = 'D';
        genID[16] = 'S';
        genID[17] = 'X';
        genID[18] = 'Z';
        genID[19] = 'V';
        genID[20] = '1';
        genID[21] = '2';
        genID[22] = '3';
        genID[23] = '4';
        genID[24] = '5';
        genID[25] = '6';
        genID[26] = '7';
        genID[27] = '8';
        genID[28] = '9';
        genID[29] = '0';
        Random rand = new Random();
        for (int I = 8; I > 0; I--) {
            int index = rand.nextInt(30);
            id = id + genID[index];
        }
        sID = id;
        try {
            Thread.currentThread().sleep(300000);
        }catch(Exception e) {}
        sIDlast=sID;
    }
    public static String getsID(){
        return sID;
    }
    public static String getsIDlast(){
        return sIDlast;
    }
}
